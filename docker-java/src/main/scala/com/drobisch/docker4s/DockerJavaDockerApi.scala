package com.drobisch.docker4s
import com.github.dockerjava.api.model._
import com.github.dockerjava.core.DockerClientBuilder
import com.github.dockerjava.core.command.{LogContainerResultCallback, PullImageResultCallback}

import scala.collection.mutable
import scala.concurrent.{Future, Promise}

class DockerJavaDockerApi extends DockerApi {
  import com.github.dockerjava.api.DockerClient
  import com.github.dockerjava.core.DefaultDockerClientConfig

  import scala.concurrent.ExecutionContext.Implicits.global

  val dockerClient: DockerClient = DockerClientBuilder.getInstance(
    DefaultDockerClientConfig.createDefaultConfigBuilder().build
  ).build()

  override def pull(imageSpec: String): Future[String] = Future {
    dockerClient.pullImageCmd(imageSpec).exec(new PullImageResultCallback).awaitSuccess()
    imageSpec
  }

  override def createContainer(config: DockerContainerConfig): Future[StoppedContainer] = Future {
    val portBindings = new Ports()

    config.portBindings.foreach {
      case (port, Some(binding)) => portBindings.bind(ExposedPort.tcp(port), Ports.Binding.bindIpAndPort(config.bindingAddress, binding))
      case (port, None) => portBindings.bind(ExposedPort.tcp(port), Ports.Binding.empty())
    }

    val createResponse = dockerClient.createContainerCmd(config.image)
      .withCmd(config.cmd: _*)
      .withEnv(config.env: _*)
      .withExposedPorts(config.exposedPorts.map(new ExposedPort(_)): _*)
      .withLinks(config.links.map(linkSpec => {
        linkSpec.split(":").toList match {
          case name :: alias :: Nil => new Link(name, alias)
          case name :: Nil => new Link(name, name)
        }
      }): _*)
      .withName(config.name)
      .withPortBindings(portBindings)
      .withNetworkMode(config.networkMode.dockerId)
      .exec()

    StoppedContainer(createResponse.getId, config)
  }

  override def startContainer(existingContainer: Container, readyLogMessage: Option[String]): Future[RunningContainer] = {
    val promise = Promise[Boolean]()

    dockerClient.startContainerCmd(existingContainer.id).exec()

    val logIterator = new LogContainerResultCallback with Iterator[String] {
      if (readyLogMessage.isEmpty) {
        promise.success(true)
      }

      val queue = mutable.Queue[String]()

      override def onNext(item: Frame): Unit = {
        val line = new String(item.getPayload)

        if (readyLogMessage.exists(line.contains(_))) {
          promise.success(true)
        }

        queue.enqueue(line)
      }

      override def hasNext: Boolean = queue.nonEmpty
      override def next(): String = queue.dequeue()
    }

    val log: LogContainerResultCallback = dockerClient.logContainerCmd(existingContainer.id)
      .withStdErr(true)
      .withStdOut(true)
      .withFollowStream(true)
      .withTailAll()
      .exec(logIterator)

    promise.future.map(_ => RunningContainer(existingContainer.id, existingContainer.config, logIterator))
  }

  override def killContainer(runningContainer: Container): Future[StoppedContainer] = Future {
    dockerClient.killContainerCmd(runningContainer.id).exec()
    StoppedContainer(runningContainer.id, runningContainer.config)
  }

  override def removeContainer(existingContainer: Container): Future[StoppedContainer] = Future {
    dockerClient.removeContainerCmd(existingContainer.id).exec()
    StoppedContainer(existingContainer.id, existingContainer.config)
  }
}
