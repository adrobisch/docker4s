docker4s
========

Library to control docker images from Scala using either the Spotify `docker-client` or `docker-java`.

License
=======

[Apache License v2.0](LICENSE)
