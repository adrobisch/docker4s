lazy val mainScalaVersion = "2.12.2"

lazy val commonSettings = Seq(
  organization := "com.drobisch",
  scalaVersion := mainScalaVersion,
  crossScalaVersions := Seq("2.11.11", mainScalaVersion),
  libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3" % Test,
  bintrayOrganization := Some("drobisch-com"),
  bintrayRepository := "docker4s",
  licenses += ("Apache-2.0", url("https://opensource.org/licenses/apache-2.0"))
)

lazy val core = (project in file(".") / "core")
  .settings(commonSettings)
  .settings(
    name := "docker4s-core"
  )

lazy val spotify = (project in file(".") / "spotify")
  .settings(commonSettings)
  .settings(
    name := "docker4s-spotify",
    libraryDependencies += "com.spotify" % "docker-client" % "8.7.1"
  ).dependsOn(core)

lazy val dockerJava = (project in file(".") / "docker-java")
  .settings(commonSettings)
  .settings(
    name := "docker4s-docker-java",
    libraryDependencies += "com.github.docker-java" % "docker-java" % "3.0.6"
  ).dependsOn(core)

lazy val docker4s = (project in file("."))
  .settings(commonSettings)
  .settings(
    publishLocal := {},
    publish := {}
  ).aggregate(core, spotify, dockerJava)
