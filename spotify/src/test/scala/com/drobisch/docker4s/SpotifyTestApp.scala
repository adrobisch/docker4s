package com.drobisch.docker4s

import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object SpotifyTestApp extends App with DockerSupport {
  lazy val dockerApi = new SpotifyDockerApi

  val log = LoggerFactory.getLogger(getClass)

  val zookeeper = DockerContainerConfig(
    name = "zookeeper",
    image = "confluentinc/cp-zookeeper:3.1.2",
    env = Seq(
      "ZOOKEEPER_TICK_TIME=2000",
      "ZOOKEEPER_CLIENT_PORT=2181"
    ),
    portBindings = Seq(
      2181 -> Some(2181)
    )
  )

  val startFuture: Future[StoppedContainer] = startContainer(zookeeper, Some("binding to port")).flatMap(started => {
    log.info(s"started ${started.id}")
    Thread.sleep(5000)
    log.info("going to stop container")
    stopContainer(started)
  }).flatMap(removeContainer(_))
}
