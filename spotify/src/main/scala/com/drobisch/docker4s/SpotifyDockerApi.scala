package com.drobisch.docker4s

import java.util

import com.spotify.docker.client.{DefaultDockerClient, LogStream}
import com.spotify.docker.client.DockerClient.AttachParameter
import com.spotify.docker.client.messages.{ContainerConfig, HostConfig, PortBinding}

import scala.concurrent.{Future, Promise}

class SpotifyDockerApi extends DockerApi {
  import scala.concurrent.ExecutionContext.Implicits.global

  val dockerClient = DefaultDockerClient.fromEnv().build()

  override def pull(imageSpec: String): Future[String] = {
    val promise = Promise[String]()
    try {
      dockerClient.pull(imageSpec)
      promise.success(imageSpec)
    } catch {
      case e: Throwable => promise.failure(e)
    }
    promise.future
  }

  def createConfig(dockerConfig: DockerContainerConfig): ContainerConfig = {
    val portBindings = new util.HashMap[String, util.List[PortBinding]]()

    dockerConfig.portBindings.map {
      case (container, Some(host)) =>
        portBindings.put(container.toString, util.Arrays.asList(PortBinding.of(dockerConfig.bindingAddress, host)))
      case (container, None) =>
        portBindings.put(container.toString, util.Arrays.asList(PortBinding.randomPort(dockerConfig.bindingAddress)))
    }

    val hostConfig = HostConfig.builder()
      .links(dockerConfig.links: _*)
      .networkMode(dockerConfig.networkMode.dockerId)
      .portBindings(portBindings)
      .build()

    ContainerConfig.builder
      .hostConfig(hostConfig)
      .image(dockerConfig.image)
      .env(dockerConfig.env: _*)
      .exposedPorts(dockerConfig.exposedPorts.map(_.toString): _*)
      .cmd(dockerConfig.cmd: _*)
      .build
  }

  override def createContainer(spec: DockerContainerConfig): Future[StoppedContainer] =
    Future(dockerClient.createContainer(createConfig(spec), spec.name))
      .map(containerCreation => StoppedContainer(containerCreation.id(), spec))

  override def killContainer(runningContainer: Container): Future[StoppedContainer] = {
    val promise = Promise[StoppedContainer]()
    try {
      dockerClient.killContainer(runningContainer.id)
      promise.success(StoppedContainer(runningContainer.id, runningContainer.config))
    } catch {
      case e: Throwable => promise.failure(e)
    }
    promise.future
  }

  override def startContainer(existingContainer: Container,
                              readyLogMessage: Option[String] = None): Future[RunningContainer] = {
    dockerClient.startContainer(existingContainer.id)

    val logs: LogStream = dockerClient
      .attachContainer(existingContainer.id,
        AttachParameter.LOGS, AttachParameter.STDOUT,
        AttachParameter.STDERR, AttachParameter.STREAM)

    val readyPromise = Promise[RunningContainer]()

    Future {
      def lineFilter(containedString: String) =
        (line: String) => line.contains(containedString)

      import scala.collection.JavaConverters._

      val readyAction: Any => Unit = _ =>
        readyPromise.success(
          RunningContainer(existingContainer.id, existingContainer.config, logs.asScala.map(_.content().toString))
        )

      // this blocks
      logs.attach(
        new LogOutputStream(logFilter = readyLogMessage.map(lineFilter), filterAction = Some(readyAction)),
        new LogOutputStream
      )
    }

    readyPromise.future
  }

  override def removeContainer(existingContainer: Container): Future[StoppedContainer] =
    Future {
      dockerClient.removeContainer(existingContainer.id)
    }.map(_ => StoppedContainer(existingContainer.id, existingContainer.config))
}
