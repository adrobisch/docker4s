package com.drobisch.docker4s

import com.drobisch.docker4s.DockerApi.LogStream
import com.drobisch.docker4s.NetworkMode.Bridge

import scala.concurrent.Future

sealed trait NetworkMode {
  def dockerId: String
}

object NetworkMode {
  case object Bridge extends NetworkMode {
    def dockerId = "bridge"
  }

  case object Host extends NetworkMode {
    def dockerId = "host"
  }

  case object None extends NetworkMode {
    def dockerId = "none"
  }
}

case class DockerContainerConfig(name: String,
                                 image: String,
                                 exposedPorts: Seq[Int] = Seq(),
                                 portBindings: Seq[(Int, Option[Int])] = Seq(),
                                 cmd: Seq[String] = Seq(),
                                 env: Seq[String] = Seq(),
                                 links: Seq[String] = Seq(),
                                 networkMode: NetworkMode = Bridge,
                                 bindingAddress: String = "0.0.0.0")

trait Container {
  def id: String
  def config: DockerContainerConfig
}

case class RunningContainer(id: String, config: DockerContainerConfig, logStream: LogStream) extends Container
case class StoppedContainer(id: String, config: DockerContainerConfig) extends Container

trait DockerApi {
  def pull(imageSpec: String): Future[String]
  def createContainer(config: DockerContainerConfig): Future[StoppedContainer]
  def startContainer(existingContainer: Container, readyLogMessage: Option[String] = None): Future[RunningContainer]
  def killContainer(runningContainer: Container): Future[StoppedContainer]
  def removeContainer(existingContainer: Container): Future[StoppedContainer]
}

object DockerApi {
  type LogFilter = String => Boolean
  type LogStream = Iterator[String]
}
