package com.drobisch.docker4s

import java.io.OutputStream

class LogOutputStream(log: String => Unit = println,
                      logFilter: Option[DockerApi.LogFilter] = None,
                      filterAction: Option[Any => Unit] = None) extends OutputStream {
  var mem = ""

  if (logFilter.isEmpty) {
    filterAction.foreach(_())
  }

  override def write(b: Int): Unit = {
    val bytes = new Array[Byte](1)
    bytes(0) = (b & 0xff).asInstanceOf[Byte]
    mem = mem + new String(bytes)

    if (mem.endsWith("\n")) {
      mem = mem.substring(0, mem.length - 1)
      flush
    }
  }

  override def flush = {
    if (logFilter.exists(_(mem))) {
      filterAction.foreach(_())
    }
    log(mem)
    mem = ""
  }
}