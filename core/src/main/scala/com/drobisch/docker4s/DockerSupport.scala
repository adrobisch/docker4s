package com.drobisch.docker4s

import scala.concurrent.Future

import scala.concurrent.ExecutionContext.Implicits.global

trait DockerSupport {
  def dockerApi: DockerApi

  def startContainer(config: DockerContainerConfig,
                     readyLogMessage: Option[String] = None): Future[RunningContainer] =
    for {
      _ <- dockerApi.pull(config.image)
      container <- dockerApi.createContainer(config)
      startedContainer <- dockerApi.startContainer(container, readyLogMessage)
    } yield startedContainer

  def stopContainer(container: Container): Future[StoppedContainer] =
    dockerApi.killContainer(container)

  def removeContainer(container: Container): Future[StoppedContainer] =
    dockerApi.removeContainer(container)
}